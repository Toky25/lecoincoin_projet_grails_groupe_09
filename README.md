# Lecoincoin_projet_grails_groupe_09

**Projet grails du groupe 09 (back-office et API)**

**Membres du groupe:**
- RAFANOMEZANA Tahiry Bakolitsiky - NUM-15
- RAFIDIMANANA Toky Ny Aina - NUM-19
- RAMANANTSOA Kevin Allain - NUM-32
- RANOMENJANAHARY Miora Gabrielle - NUM-46
- RATSIMANDAVANA Rantotiana Riantsoa - NUM-51


**########## Notes avant de lancer le projet ######################################################################**

- Veuillez utiliser le port 8080 pour lancer le projet (il est déjà configuré par défaut dans l'application)
- Veuillez définir le chemin du projet vers le répertoire des images dans application.yml
(Le chemin par défaut dans le projet est "D:/ProjetGrailsGroupe/Grails-Toky/Lecoincoin/grails-app/assets/images/")
- Lorsque vous allez uploader plusieurs images dans une annonce, il faudrait les uploader d'un seul coup
- Lien en local de l'appplication: http://localhost:8080/
- Les données de Login Admin du site (login: admin et mot de passe: password), ces informations se trouvent dans le fichier Bootstrap 



**########## Fonctionnalités ###################################################################################**

**Gestion des annonces et des illustrations:**
- Liste paginée des annonces
- Recherche des annonces par titre
- Ajout d'une annonce avec la possibilité d'uploader des images pour l'illustration de l'annonce
- Modification d'une annonce avec les images d'illustrations
- Suppression d'une annonce en cascade avec ces illustrations


**Gestion des utilisateurs avec leur rôles:**
- Liste paginée des utilisateurs
- Ajout d'utilisateur avec l'ajout de son role (soit role_admin, role_modo ou bien role_user) dans la table "UserRole" de la base de données
- Modification d'un utilisateur avec son rôle
- Suppression d'un utilisateur en cascade avec son rôle

	
**API**
- Pour tester la collection d’api, l’importer d’abord dans « postman ». Les requête suivent déjà un ordre :
- commencer par exécuter la première requête qui consiste à récupérer un « token ». 
- ensuite coller ce  « token » dans l’onglet autorisation de chaque requête qui suit, choisir l’option « bearer token » dans le menu déroulant.

	



