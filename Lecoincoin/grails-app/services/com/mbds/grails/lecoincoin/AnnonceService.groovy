package com.mbds.grails.lecoincoin

import grails.converters.JSON
import grails.gorm.services.Service
import grails.gorm.transactions.Transactional
import org.springframework.validation.BindingResult

@Transactional
class AnnonceService {

    @Transactional
    def save(Annonce annonce){
        try{
            println(annonce as JSON)
            annonce.save()
        }catch(Exception e){
            throw e
        }
    }

    def getAllAnnonce(){
        return Annonce.list()
    }

    def update(Annonce annonce, int idAnnonce){
        try{
            Annonce nouveau_annonce = Annonce.get(idAnnonce)
            nouveau_annonce.title = annonce.title
            nouveau_annonce.description = annonce.description
            nouveau_annonce.price = annonce.price
            nouveau_annonce.dateUpdated = annonce.dateUpdated
            for(illustration in annonce.illustrations){
                nouveau_annonce.addToIllustrations(illustration)
            }
            //println(nouveau_annonce.illustrations as JSON)
            def r = nouveau_annonce.save()
            if (!r) {
                nouveau_annonce.errors.allErrors.each {
                    throw new Exception(it.toString())
                }
            }
        }catch(Exception e){
            throw e
        }
    }


    @Transactional(readOnly = true)
    def getAllPagginate(int max, int offset){
        try{
            def liste = Annonce.createCriteria().list(max:max,offset:offset){
                order("id", "desc")
            }
            return liste
        }catch(Exception e){
            throw e
        }
    }

    @Transactional(readOnly = true)
    def count(){
        try{
            return Annonce.count()
        }catch(Exception e){
            throw e
        }
    }

    @Transactional(readOnly = true)
    def countRecherche(def key){
        try{
            List<Annonce> liste = Annonce.withCriteria {
                ilike("title", '%'+key+'%')
            }
            return liste.size()
        }catch(Exception e){
            throw e
        }
    }


    @Transactional(readOnly = true)
    def getById(def id){
        try{
            return Annonce.get(id)
        }catch(Exception e){
            throw e
        }
    }

    @Transactional
    def delete(def id){
        try{
            def annonce = Annonce.get(id)
            annonce.delete()
        }catch(Exception e){
            throw e
        }
    }

    @Transactional
    def deleteIllustrationByIdAnnonce(def id){
        try{
            def illustration = Illustration.get(id)
            illustration.delete()
        }catch(Exception e){
            throw e;
        }
    }

    @Transactional(readOnly = true)
    def rechercher(def key, int max, int offset){
        def liste = Annonce.createCriteria().list(max:max,offset:offset){
            ilike("title", '%'+key+'%')
        }
        return liste
    }

}