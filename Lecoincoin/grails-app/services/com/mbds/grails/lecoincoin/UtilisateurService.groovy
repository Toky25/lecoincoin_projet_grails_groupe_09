package com.mbds.grails.lecoincoin

import grails.gorm.transactions.Transactional

@Transactional
class UtilisateurService {

    def listeUtilisateur() {
        return User.list()
    }

    def getUserRole(){
        return UserRole.getAll()
    }

    def compareUserRoleToUser(List<UserRole> listeUserRole,def idutilisateur){
        def idrole;
        for(int i=0;i<listeUserRole.size();i++){
            if(listeUserRole[i].userId == idutilisateur){
                idrole = listeUserRole[i].roleId
            }
        }
        return idrole
    }


    @Transactional(readOnly = true)
    def getAllPagginate(int max, int offset){
        try{
            def liste = User.list(max:max,offset:offset)
            return liste
        }catch(Exception e){
            throw e
        }
    }

    @Transactional(readOnly = true)
    def count(){
        try{
            return User.count()
        }catch(Exception e){
            throw e
        }
    }

    def listeRole() {
        return Role.list()
    }

    def getUtilisateur(Serializable id) {
        return User.get(id)
    }

    def getUtilisateurRole(User utilisateur){
        return utilisateur.getAuthorities()
    }

    @Transactional
    def save(User utilisateur){
        try{
            utilisateur.save(flush: true)
            if (!utilisateur.save()) {
                utilisateur.errors.allErrors.each {
                    throw new Exception("L'enregistrement de l'utilisateur n'a pas été effectué!")
                }
            }
        }catch(Exception e){
            throw e
        }

    }

    @Transactional
    def delete(def id){
        try{
            def utilisateur = User.get(id)
            utilisateur.delete()
            UserRole.removeAll(utilisateur)
        }catch(Exception e){
            throw e
        }
    }

    @Transactional
    def modifierUtilisateur(User utilisateur, def nom, def password, Long idRole){
        def map = [username: nom, password: password]

        try{
            utilisateur.properties = map
            utilisateur.save()

            UserRole.removeAll(utilisateur)
            def role = getRole(idRole)
            UserRole userRole = new UserRole(
                    user: utilisateur,
                    role: role
            )
            saveUserRole(userRole)


        }catch(Exception e){
            throw e
        }
    }

    def getUserRoleByIdUser (long idUser){
        return UserRole.findByUser(User.get(idUser))
    }

    @Transactional
    def saveUserRole(UserRole userRole){
        try{
            userRole.save()
            if (!userRole.save()) {
                userRole.errors.allErrors.each {
                    throw new Exception("L'enregistrement de l'userRole n'a pas été effectué!")
                }
            }
        }catch(Exception e){
            throw e
        }
    }

    @Transactional
    def deleteUserRole(User user){
        try{
            UserRole.removeAll(user)
        }catch(Exception e){
            throw e
        }
    }

    def getRole(Serializable id) {
        return Role.get(id)
    }

    @Transactional
    def modifierUtilisateurPartiellement(User utilisateur, def nom, def password){
        def map = [username: nom, password: password]

        try{
            utilisateur.properties = map
            utilisateur.save()
        }catch(Exception e){
            throw e
        }
    }

}
