package lecoincoin

import com.mbds.grails.lecoincoin.Annonce
import com.mbds.grails.lecoincoin.Illustration
import com.mbds.grails.lecoincoin.Role
import com.mbds.grails.lecoincoin.User
import com.mbds.grails.lecoincoin.UserRole

class BootStrap {

    def annonceService
    def init = { servletContext ->

        def adminRole = new Role(authority: "ROLE_ADMIN").save()
        def modRole = new Role(authority: "ROLE_MODO").save()
        def userRole = new Role(authority: "ROLE_USER").save()

        def adminUser = new User(username: "admin", password: "password").save()
        def modUser = new User(username: "moderateur", password: "password").save()
        def userUser = new User(username: "client", password: "password").save()

        UserRole.create adminUser, adminRole, true
        UserRole.create(modUser, modRole, true)
        UserRole.create(userUser, userRole, true)

        def annonce1 = new Annonce(price: 30,description: "Zack Snyder's Justice League (Date provisoire. Sortie Prochaine) [4K Ultra HD + Blu-Ray-Édition boîtier SteelBook] ", title: "Zack Snyder's Justice League",dateUpdated: new Date(),dateCreated: new Date())
        annonce1.addToIllustrations(new Illustration(filename: "JL1.jpg"))
        annonce1.addToIllustrations(new Illustration(filename: "JL.jpg"))
        annonce1.setAuthor(User.get(1))
        annonceService.save(annonce1)

        def annonce2 = new Annonce(price: 699,description: "Cet appareil photo hybride vous incitera à créer des histoires que vous serez fier de partager grâce à des technologies comme la vidéo 4K, un capteur digne d'un reflex et une connexion sans effort. Prise de vue simple pour d'excellents résultats, Prise en main, confortable et intuitive, Connexion et contrôle via WiFi* et Bluetooth, Technologies récentes au service de vos histoires, Liberté créative pour immortaliser chaque moment ", title: "Pack Fnac Hybride Canon EOS M50 Noir + Objectif EF-M 15-45 mm f/3.5-6.3 IS STM + ",dateUpdated: new Date(),dateCreated: new Date())
        annonce2.addToIllustrations(new Illustration(filename: "Canon1.jpg"))
        annonce2.addToIllustrations(new Illustration(filename: "Canon.jpg"))
        annonce2.setAuthor(User.get(1))
        annonceService.save(annonce2)

        def annonce3 = new Annonce(price: 499,description: "La console PS5™ recèle de possibilités de jeu inédites. Découvrez des temps de chargement accélérés grâce au disque SSD ultra rapide, une immersion plus poussée grâce au retour haptique, aux gâchettes adaptatives et au son 3D, sans oublier un catalogue exceptionnel de jeux PlayStation® nouvelle génération. ", title: "Console Sony PS5 Edition Standard",dateUpdated: new Date(),dateCreated: new Date())
        annonce3.addToIllustrations(new Illustration(filename: "PS51.jpg"))
        annonce3.addToIllustrations(new Illustration(filename: "PS5.jpg"))
        annonce3.setAuthor(User.get(1))
        annonceService.save(annonce3)

        def annonce4 = new Annonce(price: 499,description: "Voici la Xbox Series X, notre console la plus rapide et la plus puissante jamais conçue, pour une génération de consoles qui vous place, vous, le joueur, au centre. Puissance, Vitesse et compatibilité: La nouvelle Xbox embarque une puce graphique de 12 teraflops, compatible avec 4 générations de jeux, alliant puissance et rapidité. Une circulation d’air optimisée: Les trois canaux de circulation d’air répartissent uniformément la chaleur générée par les composants, garantissant une console silencieuse. L’architecture innovante de refroidissement parallèle permet de ne pas faire de compromis entre des performances incroyables et un confort d’usage. Plus de compromis sur le stockage: La carte d’extension de stockage de la Xbox Series X fournit un stockage de jeu supplémentaire, et offre une vitesse et des performances optimales en reproduisant l’expérience SSD personnalisée interne de la console. La carte de 1 To est insérée directement à l’arrière de la console, dans le port d’extension de stockage dédié. Optimisé pour Xbox Series X: Les jeux optimisés pour Xbox Series X sont conçus pour tirer parti des capacités uniques de la console. Certains jeux Xbox One seront optimisés Series X. Ils présenteront des niveaux inégalés de temps de chargement, de visuels, de réactivité et de fréquences d’images jusqu’à 120 FPS.", title: "Console Microsoft Xbox Series X Noir ",dateUpdated: new Date(),dateCreated: new Date())
        annonce4.addToIllustrations(new Illustration(filename: "XboxX1.jpg"))
        annonce4.addToIllustrations(new Illustration(filename: "XboxX.jpg"))
        annonce4.setAuthor(User.get(1))
        annonceService.save(annonce4)

        def annonce5 = new Annonce(price: 20,description: "Hades, le rogue-like à succès, élu Meilleur jeu d’action et Meilleur jeu indépendant aux Game Awardsde 2020 arrive en version physique sur Nintendo Switch. En plus du jeu d’origine, un code de téléchargement pour la bande originale du jeu ainsi qu’un artbook de 32 pages seront présents dans une version spéciale, disponible en édition limitée. Dans la peau du Prince des Enfers, et entouré par les Dieux de l’Olympe, apprenez à manier des armes mythiques pour vous libérer des griffes du Dieu des Morts en personne, Hadès, en développant vos compétences à chaque nouvelle évasion. Tentez de vous échapper des enfers pour la toute première fois, ou pour la centième, avec la sortie physique d’Hades en édition collector limitée, le 19 mars sur Nintendo Switch et Nintendo Switch Lite.", title: "Hades Edition Collector Nintendo Switch",dateUpdated: new Date(),dateCreated: new Date())
        annonce5.addToIllustrations(new Illustration(filename: "Hades-Nintendo-Switch1.jpg"))
        annonce5.addToIllustrations(new Illustration(filename: "Hades-Nintendo-Switch.jpg"))
        annonce5.setAuthor(User.get(1))
        annonceService.save(annonce5)

        def annonce6 = new Annonce(price: 500,description: "Grand écran ultra fluide,Des photos lumineuses : de jour comme de nuit,Space Zoom x30 : Zoom optique hybride x3,Prêt pour le futur : compatible 5G ", title: "Smartphone Samsung Galaxy S20FE 6.5 Double SIM 5G 128 Go Bleu ",dateUpdated: new Date(),dateCreated: new Date())
        annonce6.addToIllustrations(new Illustration(filename: "S201.jpg"))
        annonce6.addToIllustrations(new Illustration(filename: "S20.jpg"))
        annonce6.setAuthor(User.get(1))
        annonceService.save(annonce6)

        def annonce7 = new Annonce(price: 79,description: "Nespresso présente Inissia, une machine accessible au design optimiste et coloré pour éveiller le quotidien. Plus petit modèle de la gamme des machines Nespresso, Inissia bénéficie de la même technologie : un système exclusif, doté d'une pompe d'extraction de 19 bars, spécialement conçu pour révéler la qualité exceptionnelle des Grands Crus Nespresso. À la maison, au bureau, en vacances. Inissia trouve sa place partout grâce à sa gamme pétillante de dix couleurs qui sera dévoilée tout au long de l'année, au fil des saisons. Un objet qui deviendra vite iconique avec notamment ses séries limitées de créateurs.", title: "Expresso à capsules Magimix Nespresso M105 Inissia crème ",dateUpdated: new Date(),dateCreated: new Date())
        annonce7.addToIllustrations(new Illustration(filename: "Expre1.jpg"))
        annonce7.addToIllustrations(new Illustration(filename: "Expre.jpg"))
        annonce7.setAuthor(User.get(1))
        annonceService.save(annonce7)

        def annonce8 = new Annonce(price: 379,
                description: "La meilleure réduction de bruit* du marché avec intelligence artificielle pour une expérience sonore immersive exceptionnelle. " +
                        "Réglable sur 20 niveaux et mode Ambient Sound pour rester à l’écoute de votre environnement. Plus de réglages intelligents grâce à l’application gratuite Sony Headphones Connect. ",
                title: "Casque audio à réduction de bruit Bluetooth Sony WH1000XM4 Noir ",
                dateUpdated: new Date(),
                dateCreated: new Date())
        annonce8.addToIllustrations(new Illustration(filename: "Sony1.jpg"))
        annonce8.addToIllustrations(new Illustration(filename: "Sony.jpg"))
        annonce8.setAuthor(User.get(1))
        annonceService.save(annonce8)

        def annonce9 = new Annonce(price: 1190,
                description: "Le vélo électrique pliable par excellence ! Équipée d’une batterie 12 Ah – 36 V, votre Urban pourra vous assister sur de longues distances. " +
                        "Capable de supporter 80 km d’autonomie avec une seule charge, les batteries nouvelle génération Velair vous permettent d’aller plus loin, plus longtemps. " +
                        "Le plus? Votre batterie Velair est entièrement transportable, afin de pouvoir la recharger sur simple prise secteur en toutes circonstances." +
                        " Votre vélo à assistance électrique Urban est pliable. En une seule manoeuvre, il pourra se ranger facilement chez vous ou bien dans votre voiture." +
                        " Son poids et sa fonction pliable vous permettra de l’emmener à chacun de vos déplacements, sans qu’il vous prenne de place." +
                        " Il facilitera tous vos déplacements professionnels comme personnels.",
                title: "Vélo électrique pliable Velair Urban 250 W Noir ",
                dateUpdated: new Date(),
                dateCreated: new Date())
        annonce9.addToIllustrations(new Illustration(filename: "velo1.jpg"))
        annonce9.addToIllustrations(new Illustration(filename: "velo.jpg"))
        annonce9.setAuthor(User.get(1))
        annonceService.save(annonce9)

        def annonce10 = new Annonce(price: 19,
                description: "Occasions universelles - Profitez d'une image plus proche, plus claire et plus lumineuse avec ce télescope monoculaire haute puissance 40x60. " +
                        "Ce monoculaire peut être utilisé où vous voulez , parfait pour l'observation de la faune comme les oiseaux et autres animaux sauvages;" +
                        " Activités comme les concerts, les jeux sportifs, les voyages; Exploration en plein air comme la randonnée, " +
                        "le camping, la pêche, la chasse, etc. monoculaire adulte a un champ de vision assez large",
                title: "Jumelle zoom",
                dateUpdated: new Date(),
                dateCreated: new Date())
        annonce10.addToIllustrations(new Illustration(filename: "Mono1.jpg"))
        annonce10.addToIllustrations(new Illustration(filename: "Mono.jpg"))
        annonce10.setAuthor(User.get(1))
        annonceService.save(annonce10)

        def annonce11 = new Annonce(price: 399,
                description: "L'excellence de l'ultra haute définition 4K Profitez d'une image 4 fois plus détaillée et précise qu'en full HD. Le nouveau Crystal processor 4k optimise le traitement de l'image et propose des couleurs plus naturelles et éclatantes. Ne manquez plus aucun détail dans les scènes lumineuses ou dans les scènes les plus sombres avec le mode HDR. L'UHD dimming optimise les contrastes avec un niveau de précision inégalé. Vivez une expérience aussi spectaculaire qu'immersive. Un design entièrement repensé Avec son design ultra slim et épuré, dissimulez discrètement les câbles dans le pied de votre téléviseur pour que vous puissiez profiter pleinement de ses lignes raffinés, sans la jungle des fils. Qu'il soit allumé ou éteint, le téléviseur s'intègre parfaitement à votre intérieur grâce à ses bords fins et son dos épuré. Avec son design ultra fin, concentrez-vous désormais sur l'image. ", title: "TV Samsung UE43TU7125 4K UHD Smart TV 43’’ Gris 2020",dateUpdated: new Date(),dateCreated: new Date())
        annonce11.addToIllustrations(new Illustration(filename: "TV1.jpg"))
        annonce11.addToIllustrations(new Illustration(filename: "TV.jpg"))
        annonce11.setAuthor(User.get(1))
        annonceService.save(annonce11)

        def annonce12 = new Annonce(price: 649,
                description: "L’aspirateur sans fil Dyson le plus puissant et intelligent.Fonctionne avec" +
                        " le moteur numérique Hyperdymium™. Le moteur Dyson Hyperdymium™ tourne jusqu’à 125 000 tours par minute pour" +
                        " une aspiration puissante.La batterie Dyson la plus puissante. La batterie lithium-ion dotée de 7 cellules fournit " +
                        "jusqu’à 60 minutes de puissance constante - même avec une brosse motorisée (en mode Eco sur sols durs)." +
                        " Batterie détachable en un clic. Ecran LCD. Affiche l’autonomie restante et la performance de l’appareil en temps réel. " +
                        "Vous permet de passer facilement en mode Eco, Auto et Boost. Brosse motorisée High Torque.Avec un système de détection dynamique de la charge qui s’adapte " +
                        "automatiquement aux différents types de sols – pour nettoyer en profondeur les tapis et optimiser l’autonomie lors de l’aspiration sur les sols durs. " +
                        "Filtration la plus avancée. * Testée conformément à la norme EN60312-1, clause 5.11 en mode Boost. ",
                title: "Aspirateur balai sans fil Dyson V11 Absolute Extra 610 W Bleu ",
                dateUpdated: new Date(),
                dateCreated: new Date())
        annonce12.addToIllustrations(new Illustration(filename: "aspi1.jpg"))
        annonce12.addToIllustrations(new Illustration(filename: "aspi.jpg"))
        annonce12.setAuthor(User.get(1))
        annonceService.save(annonce12)

        def annonce13 = new Annonce(price: 17,
                description: "Tout le monde s'amuse avec UNO ! Le jeu de cartes si populaire, qui consiste à associer des couleurs ou des chiffres, comprend désormais des cartes Joker personnalisables ! Tour à tour, les joueurs tentent de se débarrasser de toutes leurs cartes en recouvrant la carte retournée en haut de la pile par une carte correspondante de leur main. Des cartes action spéciales rythment le jeu en vous aidant à battre vos adversaires. Utilisez la carte Changement de main pour échanger votre main avec celle de n'importe quel autre joueur, et servez-vous des 3 cartes Joker personnalisables (et effaçables) pour inventer vos propres règles. Le jeu comprend 19 cartes de chaque couleur (rouge, vert, bleu et jaune), 8 cartes + 2, Inversion et Passer de chaque couleur, ainsi que 4 cartes Joker, 4 cartes Super Joker, 1 carte Changement de main et 3 cartes à customiser. Si le joueur ne possède pas de carte lui permettant de jouer, il tire une carte de la pioche. Quand il ne reste plus qu'une carte, n'oubliez pas de crier « UNO ! » Le joueur qui se débarrasse en premier de toutes ses cartes se voit attribuer les points correspondant aux cartes que ses adversaires ont encore en main. Le vainqueur est le premier joueur à atteindre 500 points. Éclatez-vous avec UNO ! Le jeu comprend 112 cartes et une règle du jeu. Les couleurs et les décorations peuvent varier. ", title: "Jeu de cartes Uno Mattel",dateUpdated: new Date(),dateCreated: new Date())
        annonce13.addToIllustrations(new Illustration(filename: "UNO1.jpg"))
        annonce13.addToIllustrations(new Illustration(filename: "UNO.jpg"))
        annonce13.setAuthor(User.get(1))
        annonceService.save(annonce13)

        def annonce14 = new Annonce(price: 43,
                description: "Achetez, vendez et négociez pour gagner la partie. Attention à la faillite, à vous de bien choisir les rues pour ruiner vos adversaires " +
                        "et être le dernier sur le plateau de jeu ! Monopoly, le plus célèbre des jeux de société, a changé certains de ses pions. " +
                        "Retrouvez les trois petits nouveaux qui vont arpenter vos rues: le T-rex, le canard et le pingouin. Monopoly : " +
                        "jeu de société où le but est d'être le dernier joueur à ne pas faire faillite. Version française. Principe du jeu : " +
                        "Avancez autour du plateau en achetant le plus de propriétés (rues, gares et services publics) possible. " +
                        "Plus vous possédez de propriétés, plus vous percevrez de loyer. Si vous êtes le dernier joueur en jeu quand tous les autres ont fait faillite, vous gagnez ! De 2 à 6 joueurs. " +
                        "Temps de la partie : environ 1 heure. Âge recommandé : à partir de 8 ans. Accessoires inclus : " +
                        "plateau de jeu, 8 pions, 28 cartes de Propriété, 16 cartes Chance, 16 cartes de communauté, 32 maisons, 12 hôtels, 2 dés, " +
                        "liasse de billets Monopoly et les règles du jeu en français. Aucune pile n'est nécessaire. Vous aimez les jeux de société et" +
                        " partager un moment en famille ou entre amis ? Monopoly est le jeu parfait à offrir en cadeau pour les enfants, filles et" +
                        " garçons de 8 ans, 9 ans, 10 ans et plus. ", title: "eadada",
                dateUpdated: new Date(),
                dateCreated: new Date())
        annonce14.addToIllustrations(new Illustration(filename: "Monopoly1.jpg"))
        annonce14.addToIllustrations(new Illustration(filename: "Monopoly.jpg"))
        annonce14.setAuthor(User.get(1))
        annonceService.save(annonce14)

        def annonce15 = new Annonce(price: 1299,
                description: "Relevez de nouveaux défis créatifs grâce à des performances avancées et des fonctionnalités innovantes conçues pour " +
                        "repousser les limites de la photographie toujours plus loin. Une qualité d'image nouvelle génération pour passera la vitesse " +
                        "supérieure de la photographie • Un capteur plein format dernière génération de 26,2 mégapixels pour un niveau de détails " +
                        "exceptionnel même dans les zones très sombres ou lumineuses même en basse lumière Des performances qui vous donnent confiance • " +
                        "Prenez des photos en toute confiance grâce aux 45 collimateurs AF de type croisé, pour une mise au point d'une extrême précision." +
                        " Immortalisez des scènes éphémères à une vitesse de 6,5 im./s Révolution intérieure, évolution extérieure • " +
                        "Une véritable référence en matière de conception de reflex, redéfinie spécialement pour vous." +
                        " Une prise en main familière, pour prendre des photos immédiatement Soyez plus créatif avec les vidéos sur reflex • " +
                        "Repoussez les limites de vos projets en réalisant des vidéos Full HD nettes et stables Adoptez de nouvelles méthodes " +
                        "de travail grâce à la technologie d'appareil photo connecté • Grâce aux technologies Bluetooth et Wi-Fi, " +
                        "contrôlez rapidement et facilement l'EOS 6D Mark II depuis votre smartphone ou tablette. Le GPS intégré permet de localiser votre position",
                title: "Reflex Canon EOS 6D Mark II Boîtier Nu Noir ",
                dateUpdated: new Date(),
                dateCreated: new Date())
        annonce15.addToIllustrations(new Illustration(filename: "EOS1.jpg"))
        annonce15.addToIllustrations(new Illustration(filename: "EOS.jpg"))
        annonce15.setAuthor(User.get(1))
        annonceService.save(annonce15)

        def annonce16 = new Annonce(price: 699,
                description: "Cet ordinateur Surface ultra léger et construit avec des matériaux haut de gamme est équipé d'un écran tactile PixelSense™ de 12,45'' au format 3:2, " +
                        "qui vous permet d'afficher plus d'informations qu'un ordinateur traditionnel et donc d'être plus efficace",
                title: "PC Ultra-Portable Microsoft Surface Laptop Go 12.4 Intel Core i5 8 Go RAM 128 Go SSD Or",
                dateUpdated: new Date(),
                dateCreated: new Date())
        annonce16.addToIllustrations(new Illustration(filename: "surface1.jpg"))
        annonce16.addToIllustrations(new Illustration(filename: "surface.jpg"))
        annonce16.setAuthor(User.get(1))
        annonceService.save(annonce16)

        def annonce17 = new Annonce(price: 62,
                description: "Envie d'une touche bohème et romantique chez vous ? Glissez-vous dans un cocon de douceur avec notre fauteuil suspendu tectake." +
                        " Dans votre salon, votre chambre ou même dans votre jardin, créez votre espace cocooning idéal pour la méditation ou la lecture. " +
                        "En lévitation sur la chaise suspendue, le stress du quotidien s'évaporera au fil des minutes pour laisser place à une sensation de quiétude intérieure. " +
                        "Son tissage délicat en coton d'inspiration macramé et son coussin d'assise moelleux s'adaptent à votre position et vous confère une assise confortable. " +
                        "Avec son poids léger, vous pourrez installer votre fauteuil suspendu tectake là où vous le souhaitez pour une ambiance relaxante.",
                title: "TecTake Fauteuil suspendu GRAZIA - beige ",
                dateUpdated: new Date(),
                dateCreated: new Date())
        annonce17.addToIllustrations(new Illustration(filename: "fauteuille1.jpg"))
        annonce17.addToIllustrations(new Illustration(filename: "fauteuille.jpg"))
        annonce17.setAuthor(User.get(1))
        annonceService.save(annonce17)

        def annonce18 = new Annonce(price: 49,
                description: "Profitez du soleil en vous laissant bercer dans un cocon de douceur !. " +
                        "Cette élégante chaise hamac a été réalisée à la main par des artisans du Salvador.Sa toile en filet est tissée selon un savoir-faire ancien. " +
                        "Elle est réalisée en 100% coton.Le tissage de sa toile, ses cordes robustes et sa barre en bois de Pin offrent une bonne répartition du poids, " +
                        "pour plus de résistance et de stabilité.Pour vous assurer confort et durabilité, cette chaise hamac a été soigneusement testée…et approuvée ! ",
                title: "Chaise hamac écrue ",
                dateUpdated: new Date(),
                dateCreated: new Date())
        annonce18.addToIllustrations(new Illustration(filename: "hamac1.jpg"))
        annonce18.addToIllustrations(new Illustration(filename: "hamac.jpg"))
        annonce18.setAuthor(User.get(1))
        annonceService.save(annonce18)

        /*User.list().each {
            User userInstance ->
                (1..5).each {
                    Integer annonceIdx ->
                        def annonceInstance = new Annonce(
                                title: "Titre de l'annonce $annonceIdx",
                                description: "Description de l'annonce $annonceIdx",
                                price: 100 * annonceIdx
                        )
                        (1..5).each {
                            annonceInstance.addToIllustrations(new Illustration(filename: "grails.svg"))
                        }
                        //userInstance.addToAnnonces(annonceInstance)

                }
                //userInstance.save(flush: true, failOnError: true)
        }*/

    }
    def destroy = {
    }
}
