package com.mbds.grails.lecoincoin

import fonctions.FonctionUtiles
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class UtilisateurController {

    def static nbpaggination = 2
    UtilisateurService utilisateurService

    @Secured(['ROLE_ADMIN', 'ROLE_MODO'])
    def index() {
        def pages = params.page
        def xy = FonctionUtiles.getXYPaggination(Integer.parseInt(pages), nbpaggination)
        def offset = xy[0]
        def max = xy[1]
        int count = utilisateurService.count()
        def nbBoutons = FonctionUtiles.getNombreBoutonsPaggination(count, nbpaggination)
        def liste = utilisateurService.getAllPagginate(Integer.valueOf(max), Integer.valueOf(offset))
        render(view: "/utilisateur/index", model: [liste: liste, nbBoutons: nbBoutons])
    }

    @Secured(['ROLE_ADMIN'])
    def redactionUser() {
        def liste = utilisateurService.listeRole()
        println(liste[0].authority)
        render(view: "/utilisateur/redactionUser", model: [liste: liste])
    }

    @Secured(['ROLE_ADMIN'])
    def save() {
        def role = utilisateurService.getRole(params.role)
        User utilisateur = new User(
                username: params.nom,
                password: params.mdp
        )
        UserRole userRole = new UserRole(
                user: utilisateur,
                role: role
        )
        try {
            utilisateurService.save(utilisateur)
            utilisateurService.saveUserRole(userRole)
            flash.message = "L'utilisateur a été bien enregistré!"
        } catch (Exception e) {
            flash.error = e.getMessage()
        }
        redirect controller:"utilisateur", action:"index", params:[page:'1']
    }

    @Secured(['ROLE_ADMIN'])
    def delete() {
        def idutilisateur = params.id
        try {
            utilisateurService.delete(idutilisateur)
            flash.message = "L'utilisateur a bien été supprimé!"
        } catch (Exception e) {
            flash.error = e.getMessage()
        }
        redirect controller:"utilisateur", action:"index", params:[page:'1']
    }

    @Secured(['ROLE_ADMIN'])
    def modificationUser() {
        def liste = utilisateurService.listeRole()
        def utilisateur = utilisateurService.getUtilisateur(params.utilisateurId)
        List<UserRole> listeUserRole = utilisateurService.getUserRole()
        def idrole  = utilisateurService.compareUserRoleToUser(listeUserRole,utilisateur.id)
        render(view: "/utilisateur/modificationUser", model: [utilisateur: utilisateur, role: listeUserRole, liste: liste,idrole:idrole])
    }

    @Secured(['ROLE_ADMIN'])
    def update() {
        try {
            println("roleId: "+params.roleId)
            User utilisateur = utilisateurService.getUtilisateur(params.id)
            utilisateurService.modifierUtilisateur(utilisateur, params.username, params.password, Long.parseLong(params.roleId))
            flash.message = "Modification de l'utilisateur effectuée avec succès"
            redirect controller:"utilisateur", action:"index", params:[page:'1']
        } catch (Exception e) {
            flash.error = e.getMessage()
            throw e
        }
    }
}
