package com.mbds.grails.lecoincoin

import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured

@Secured('permitAll')
class LogoutController {

    static allowedMethods = [ifconnected: 'GET',logout: 'GET']
    SpringSecurityService springSecurityService

    def index() {}


    def ifconnected(){
        def response
        User user = springSecurityService.currentUser
        if(user!=null){
            response = [
                    'connected': "yes"
            ]
        }else {
            response = [
                    'connected': "no"
            ]
        }
        render response as JSON
    }

    def logout() {
        redirect uri: SpringSecurityUtils.securityConfig.logout.filterProcessesUrl
    }
}
