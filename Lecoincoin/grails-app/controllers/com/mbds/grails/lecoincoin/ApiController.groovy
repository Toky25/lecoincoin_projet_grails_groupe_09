package com.mbds.grails.lecoincoin

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured
import grails.web.JSONBuilder

import javax.servlet.http.HttpServletResponse
import javax.xml.ws.Response
import java.text.SimpleDateFormat

@Secured('ROLE_ADMIN')
class ApiController {
    AnnonceService a = new AnnonceService()
    UtilisateurService u = new UtilisateurService()
    def builder = new JSONBuilder()
    def pattern = "dd/MM/yyyy"

    def getById(idAnnonce){
        JSON.registerObjectMarshaller(Annonce){
            def output = [:]
            output['dateCreated'] = it.dateCreated
            output['dateUpdated'] = it.dateUpdated
            output['description'] = it.description
            output['id'] = it.id
            output['price'] = it.price
            output['author'] = it.author
            output['title'] = it.title
            output['illustrations'] = it.illustrations
            return output
        }
        Annonce annonce = a.getById(idAnnonce)
        render annonce as JSON
    }
//    GET / PUT / PATCH / DELETE
//    url : localhost:8081/projet/api/annonce(s)/{id}
    def annonce() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = getById(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                def json = builder.build {
                    annonceInstance
                }
                render(status: 200, contentType: 'application/json', text: json)
                return response.status = 200
            case "PUT":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = a.getById(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
//
                if(request.JSON.title && request.JSON.description && request.JSON.price && request.JSON.author) {
                    annonceInstance.title = request.JSON.title
                    annonceInstance.description = request.JSON.description
                    annonceInstance.price = Double.parseDouble(request.JSON.price)
                    annonceInstance.dateUpdated = new Date()
                    annonceInstance.author = u.getUtilisateur(request.JSON.author)
                    a.save(annonceInstance)
                    def json = builder.build {
                        message = " update success"
                    }
                    render(status: 200, contentType: 'application/json', text: json)
                    return response.status = 200
                }
            case "PATCH":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = a.getById(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
//
                if(request.JSON.description && request.JSON.price ){
                    annonceInstance.description = request.JSON.description
                    annonceInstance.price = Double.parseDouble(request.JSON.price)
                    annonceInstance.dateUpdated = new Date()
                    a.save(annonceInstance)
                    def json = builder.build {
                        message = " update success"
                    }
                    render(status: 200, contentType: 'application/json', text: json)
                    return response.status = 200
                }
            case "DELETE":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                a.delete(params.id)
                def json = builder.build {
                    message = " delete success"
                }
                render(status: 200, contentType: 'application/json', text: json)
                return response.status = 200
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / POST
    def annonces() {
        switch (request.getMethod()) {
            case "GET":
                def annonceInstance = a.getAllAnnonce()
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                def json = builder.build {
                    annonceInstance
                }
                render(status: 200, contentType: 'application/json', text: json)
                return response.status = 200
            case "POST":
                if(request.JSON.title && request.JSON.description && request.JSON.price && request.JSON.author){
                    Annonce aa = new Annonce()
                    aa.title = request.JSON.title
                    aa.description = request.JSON.description
                    aa.price = Double.parseDouble(request.JSON.price)
                    aa.author = u.getUtilisateur(request.JSON.author)
                    aa.dateCreated = Date.parse(pattern,"10/03/2021")
                    aa.dateUpdated = new Date()
                    Illustration[] list = request.JSON.Illustration
                    for(int i=0;i<list.size();i++){
                        aa.addToIllustrations(new Illustration(filename:list[i]["filename"]))
                    }
                    a.save(aa)
                    def json = builder.build {
                        message = " insert success"
                    }
                    render(status: 201, contentType: 'application/json', text: json)
                    return response.status = 201
                }
                return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }



//    GET / PUT / PATCH / DELETE
    def user() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = u.getUtilisateur(params.id)
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                def json = builder.build {
                    userInstance
                }
                render(status: 200, contentType: 'application/json', text: json)
                return response.status = 200
            case "PUT":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = u.getUtilisateur(params.id)
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND

                if(request.JSON.username && request.JSON.password && request.JSON.idrole) {
                    print("Ato")
                    u.modifierUtilisateur(userInstance,request.JSON.username,request.JSON.password,request.JSON.idrole)
                    def json = builder.build {
                        message = " update success"
                    }
                    render(status: 200, contentType: 'application/json', text: json)
                    return response.status = 200
                }
            case "PATCH":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = u.getUtilisateur(params.id)
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                if(request.JSON.username && request.JSON.password) {
                    u.modifierUtilisateurPartiellement(userInstance,request.JSON.username,request.JSON.password)
                    def json = builder.build {
                        message = " update success"
                    }
                    render(status: 200, contentType: 'application/json', text: json)
                    return response.status = 200
                }

            case "DELETE":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                u.delete(params.id)
                def json = builder.build {
                    message = " delete success"
                }
                render(status: 200, contentType: 'application/json', text: json)
                return response.status = 200
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / POST
    def users() {
        switch (request.getMethod()) {
            case "GET":
                def userInstance = u.listeUtilisateur()
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                def json = builder.build {
                    userInstance
                }
                render(status: 200, contentType: 'application/json', text: json)
                return response.status = 200
            case "POST":
                if(request.JSON.username && request.JSON.password && request.JSON.idrole){
                    User uu = new User()
                    uu.username = request.JSON.username
                    uu.password = request.JSON.password
                    UserRole userRole = new UserRole(
                            user: uu,
                            role: request.JSON.idrole
                    )
                    u.save(uu)
                    u.saveUserRole(userRole)
                    def json = builder.build {
                        message = " insert success"
                    }
                    render(status: 201, contentType: 'application/json', text: json)
                    return response.status = 201
                }
                return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

}