package com.mbds.grails.lecoincoin

import fonctions.FonctionUtiles
import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.web.multipart.MultipartFile

import java.text.SimpleDateFormat


@Secured(['ROLE_ADMIN', 'ROLE_MODO'])
class AnnonceController {

    AnnonceService annonceService
    def static nbpaggination = 5

    SpringSecurityService springSecurityService

    @Secured(['ROLE_ADMIN', 'ROLE_MODO'])
    def index() {
        def liste
        def pages = params.page
        def xy = FonctionUtiles.getXYPaggination(Integer.parseInt(pages), nbpaggination)
        def offset = xy[0]
        def max = xy[1]
        def nbBoutons

        if(!params.cle){
            liste = annonceService.getAllPagginate(Integer.valueOf(max), Integer.valueOf(offset))
            int count = annonceService.count()
            nbBoutons = FonctionUtiles.getNombreBoutonsPaggination(count, nbpaggination)
        }else{
            liste = annonceService.rechercher(params.cle, Integer.valueOf(max), Integer.valueOf(offset))
            int count = annonceService.countRecherche(params.cle)
            nbBoutons = FonctionUtiles.getNombreBoutonsPaggination(count, nbpaggination)
        }

        render(view: "/annonce/index", model: [liste: liste, nbBoutons: nbBoutons])
    }
    @Secured(['ROLE_ADMIN', 'ROLE_MODO'])
    def redactionAnnonce() {
        int id = params.annonceId as Integer
        if(id != 0){
            def idAnnonce = params.annonceId
            JSON.registerObjectMarshaller(Annonce){
                def output = [:]
                output['illustrations'] = it.illustrations
                return output
            }
            Annonce annonce = annonceService.getById(idAnnonce)
            render(view: "/annonce/redactionAnnonce", model: [annonceId: idAnnonce, ill:annonce.illustrations as JSON])
        }else{
            render(view: "/annonce/redactionAnnonce", model: [annonceId: 0])
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_MODO'])
    def save(){
        def pattern = "yyyy-MM-dd"
        User user = springSecurityService.currentUser
        Annonce annonce = new Annonce(
                title: params.titre,
                description: params.description,
                dateUpdated: new SimpleDateFormat(pattern).parse(params.dateCreation),
                price: params.prix
        )
        try{
            MultipartFile[] file = request.getFiles("image_uploads")
            for(int i=0;i<file.size();i++) {
                String namefile = file[i].originalFilename
                if(namefile!=""){
                    file[i].transferTo(new File(grailsApplication.config.annonces.illustrations.path + namefile))
                    annonce.addToIllustrations(new Illustration(filename: namefile))
                }
            }
            annonce.setAuthor(user)
            if(params.annonce as Integer!=0){
                annonceService.update(annonce,params.annonce as Integer)
                flash.message = "La modification de l'annonce a été bien enregistré!"
            }else{
                if(user.authorities.first().id==2){
                    flash.error = "Vous n'avez pas le droit de créer une annonce"
                }else if (user.authorities.first().id==1){
                    annonceService.save(annonce)
                    flash.message = "L'annonce a été bien enregistré!"
                }

            }

        }catch(Exception e){
            flash.error = e.getMessage()
        }
        redirect controller:"annonce",action:"index",params:[page:'1']

    }

    @Secured(['ROLE_ADMIN'])
    def delete(){
        def id = params.id
        try{
            annonceService.delete(id)
            flash.message = "L'annonce a bien été supprimé!"
        }catch(Exception e){
            flash.error = e.getMessage()
        }
        redirect controller:"annonce",action:"index",params:[page:'1']
    }

    @Secured(['ROLE_ADMIN', 'ROLE_MODO'])
    def getById(){
        def idAnnonce = params.idAnnonce

        JSON.registerObjectMarshaller(Annonce){
            def output = [:]
            output['dateCreated'] = it.dateCreated
            output['dateUpdated'] = it.dateUpdated
            output['description'] = it.description
            output['id'] = it.id
            output['price'] = it.price
            output['author'] = it.author.id
            output['title'] = it.title
            output['illustrations'] = it.illustrations
            return output
        }
        Annonce annonce = annonceService.getById(idAnnonce)
        render annonce as JSON
    }

    @Secured(['ROLE_ADMIN', 'ROLE_MODO'])
    def deleteFile(){
        def filename = params.key as String
        //def nomdufichier = filename.split(";")[0]
        def id = filename.split(";")[1]
        annonceService.deleteIllustrationByIdAnnonce(id as Integer)
        //def file = new File(grailsApplication.config.annonces.illustrations.path + filename)
        //file.delete()
        def responseData = [
                'results': filename+" a bien été supprimé!"
        ]
        render responseData as JSON
    }
}
