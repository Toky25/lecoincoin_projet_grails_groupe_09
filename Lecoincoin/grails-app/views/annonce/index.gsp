<!DOCTYPE html>
<head>
    <meta name="layout" content="main"/>
</head>
<div class="animated fadeIn">

    <div class="clearfix"></div>

    <br>
    <!-- Orders -->
    <div class="orders">
        <div class="row">
            <div class="col-xl-12">
                <div class="input-group col col-sm-4">
                    <g:if test="${!params.cle}">
                        <input type="text" id="cle" name="cle" placeholder="Rechercher une annonce..." class="form-control">
                    </g:if>
                    <g:else>
                        <input type="text" value="${params.cle}" id="cle" name="cle" placeholder="Rechercher une annonce..." class="form-control">
                    </g:else>

                        <div class="input-group-btn">
                            <button class="btn btn-primary" onclick="rechercher()">
                                <i class="fa fa-search"></i>
                            </button>
                            <h3>${params.result_null}</h3>
                        </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body">
                        <g:if test="${flash.message}">
                            <div class="message" style="display: block"></div>
                            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                                <span class="badge badge-pill badge-success">Succés</span>
                                ${flash.message}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        </g:if>

                        <g:if test="${flash.error}">
                            <div class="message" style="display: block"></div>
                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                <span class="badge badge-pill badge-danger">Erreur</span>
                                ${flash.error}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        </g:if>
                        <div class="col-sm-12">
                            <h4 class="d-inline-block">Annonce</h4>

                            <!--<div class="float-right"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#mediumModal" >Insérer nouveau</button></div>-->
                            <div class="float-right"><a class="btn btn-success" href="${createLink(action: 'redactionAnnonce', controller: 'annonce', params: [annonceId:0])}" >Insérer nouveau annonce</a></div>
                        </div>
                    </div>
                    <div class="card-body--">
                        <div class="table-stats order-table ov-h">
                            <table class="table ">
                                <thead>
                                <tr>
                                    <th class="serial">#</th>
                                    <th>Titre</th>
                                    <th>Prix</th>
                                    <th>Date de creation</th>
                                    <th>Date de dernière modification</th>
                                    <th>Modifier / Détails</th>
                                    <th>Supprimer</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <g:each in="${liste}" status="i" var="annonce">
                                        <tr>
                                            <td class="serial">${i+1}</td>
                                            <td><strong>${annonce.title}</strong></td>
                                            <td><strong>${annonce.price}$</strong></td>
                                            <td><g:formatDate format="yyyy-MM-dd" date="${annonce.dateCreated}"/></td>
                                            <td><g:formatDate format="yyyy-MM-dd" date="${annonce.dateUpdated}"/></td>
                                            <td><a href="${createLink(action: 'redactionAnnonce', controller: 'annonce', params: [annonceId:annonce.id])}" class="btn btn-primary">Modifier / Détails</a></td>
                                            <td><a href="${createLink(action: 'delete', controller: 'annonce', params:[id: "${annonce.id}"] )}"
                                                   onclick="return confirm('Voulez vous réellement supprimer?');" class="btn btn-danger">Supprimer</a>
                                            </td>
                                        </tr>
                                    </g:each>
                                </tbody>
                            </table>
                        </div> <!-- /.table-stats -->
                    </div>
                </div> <!-- /.card -->
            </div>  <!-- /.col-lg-8 -->
        </div>
    </div>
    <div class="float-right">
        <ul class="pagination">
            <g:if test="${Integer.valueOf(params.page)-1>0}">
                <g:if test="${!params.cle}">
                    <li class="page-item"><a class="page-link" href="${createLink(action: 'index', controller: 'annonce', params: [page:"${Integer.valueOf(params.page)-1}"])}">Précédent</a></li>
                </g:if>
                <g:else>
                    <li class="page-item"><a class="page-link" href="${createLink(action: 'index', controller: 'annonce', params: [page:"${Integer.valueOf(params.page)-1}", cle:"${params.cle}"])}">Précédent</a></li>
                </g:else>
            </g:if>
            <g:else>
                <li class="page-item disabled"><a class="page-link" href="">Précédent</a></li>
            </g:else>

            <g:set var="counter" value="${0}"/>
            <g:while test="${counter < nbBoutons}">
                <g:set var="counter" value="${counter+1}"/>
                <g:if test="${Integer.valueOf(counter)==Integer.valueOf(params.page)}">
                    <g:if test="${!params.cle}">
                        <li class="page-item active"><a class="page-link" href="${createLink(action: 'index', controller: 'annonce', params: [page:"${counter}"])}">${counter}</a></li>
                    </g:if>
                    <g:else>
                        <li class="page-item active"><a class="page-link" href="${createLink(action: 'index', controller: 'annonce', params: [page:"${counter}", cle:"${params.cle}"])}">${counter}</a></li>
                    </g:else>
                </g:if>
                <g:else>
                    <g:if test="${!params.cle}">
                        <li class="page-item"><a class="page-link" href="${createLink(action: 'index', controller: 'annonce', params: [page:"${counter}"])}">${counter}</a></li>
                    </g:if>
                    <g:else>
                        <li class="page-item"><a class="page-link" href="${createLink(action: 'index', controller: 'annonce', params: [page:"${counter}", cle:"${params.cle}"])}">${counter}</a></li>
                    </g:else>
                </g:else>
            </g:while>

            <g:if test="${Integer.valueOf(params.page)+1<=nbBoutons}">
                <g:if test="${!params.cle}">
                    <li class="page-item"><a class="page-link" href="${createLink(action: 'index', controller: 'annonce', params: [page:"${Integer.valueOf(params.page)+1}"])}">Suivant</a></li>
                </g:if>
                <g:else>
                    <li class="page-item"><a class="page-link" href="${createLink(action: 'index', controller: 'annonce', params: [page:"${Integer.valueOf(params.page)+1}", cle:"${params.cle}"])}">Suivant</a></li>
                </g:else>
            </g:if>
            <g:else>
                <li class="page-item disabled"><a class="page-link" href="">Suivant</a></li>
            </g:else>
        </ul>
    </div>
    <!-- /.orders -->
    <!-- To Do and Live Chat -->
</div>

<script>
    function rechercher(){
        if($("#cle").val().length>0){
            location.replace("${grailsApplication.config.environments.development.server.url}annonce/index?page=1&cle="+$("#cle").val());
        }else{
            location.replace("${grailsApplication.config.environments.development.server.url}annonce/index?page=1");
        }
    }
    $(document).ready(function(){
        $("#annonce").addClass("active");
    });
</script>