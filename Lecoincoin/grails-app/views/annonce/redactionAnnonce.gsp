<!DOCTYPE html>
<head>
    <meta name="layout" content="main"/>
</head>
<style>
.fileinput-remove,
.fileinput-upload{
    display: none;
}
</style>
<div class="clearfix"></div>

<br>

<!-- Orders -->
<div class="orders">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">

                <div class="card-body">
                    <g:if test="${flash.message}">
                        <div class="message" style="display: block"></div>
                        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                            <span class="badge badge-pill badge-success">Succés</span>
                            ${flash.message}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </g:if>

                    <g:if test="${flash.error}">
                        <div class="message" style="display: block"></div>
                        <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                            <span class="badge badge-pill badge-danger">Erreur</span>
                            ${flash.error}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </g:if>

                    <h3><strong>Insérer / Editer une annonce</strong></h3>
                    <br>
                    <g:form controller="annonce" enctype="multipart/form-data">
                        <div class="card-body card-block">
                            <input type="hidden" value="0" name="annonce" id="id">
                            <div class="row form-group">
                                    <label class=" form-control-label"><strong>Titre</strong></label>
                                    <input type="text" id="titre" name="titre" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label"><strong>Description</strong></label>
                                <textarea id="editor" name="description"></textarea>
                            </div>
                            <div class="row form-group">
                                <label class=" form-control-label"><strong>Date de modification</strong></label>
                                <input type="date" id="dateCreation" name="dateCreation" class="form-control" required>
                            </div>
                            <div class="row form-group">
                                <label class=" form-control-label"><strong>Prix</strong></label>
                                <input type="number" id="prix" name="prix" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label class=" form-control-label"><strong>Images d'illustration</strong></label><br>
                                <!--<input type="file" id="image_uploads" name="image_uploads" accept=".jpg, .jpeg, .png" required multiple>-->
                                <div class="file-loading">
                                     <!--<input id="image_uploads" name="image_uploads" type="file" multiple class="file" data-overwrite-initial="false">-->
                                     <input id="image_uploads" name="image_uploads" type="file" multiple>
                                </div>
                                <br>
                            </div>

                            <div class="row" id="result">
                            <!--Pour mettre les images-->
                            </div>
                        <div class="modal-footer">
                        <g:actionSubmit type="button" class="btn btn-primary" value="Confirmer" action="save"/>
                        </div>
                    </g:form>

                </div>
                </div> <!-- /.card -->
            </div>  <!-- /.col-lg-8 -->
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script>


    var url_Image = [] ;
    var filename = [];

    $(document).ready(function(){
            $("#annonce").addClass("active");
            var idAnnonce= typeof(${params.annonceId}) == 'undefined'? 0: ${params.annonceId};
                if(idAnnonce===0){
                    document.getElementById('dateCreation').valueAsDate = new Date();
                }else{
                    $.get("${grailsApplication.config.environments.development.server.url}annonce/getById?idAnnonce="+idAnnonce, function(data, status){
                        $("#id").val(data.id);
                        $("#titre").val(data.title);
                        $("#editor").val(data.description);
                        var dateFormat = new Date(data.dateUpdated);
                        $("#dateCreation").val(formatDate(dateFormat));
                        $("#prix").val(data.price);
                    });
                }
    });

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    if("${params.annonceId}"!=="0"){
        var illustrations = JSON.parse("${ill.encodeAsJSON()}".replace(/&quot;/g,'"'));
            illustrations.forEach(function(element){
                url_Image.push('${grailsApplication.config.environments.development.server.url}assets/'+element.filename)
                filename.push({
                    "key":element.filename+";"+element.id,
                })
            }
        );
    }

    $("#image_uploads").fileinput({
        initialPreview: url_Image,
        initialPreviewAsData: true,
        theme: 'fa',
        uploadUrl:"${grailsApplication.config.environments.development.server.url}assets/",
        deleteUrl: "${grailsApplication.config.environments.development.server.url}annonce/deleteFile",
        allowedFileExtensions: ['jpg', 'png', 'gif', 'jpeg', 'webp'],
        overwriteInitial: false,
        maxFileSize:2048,
        initialPreviewConfig: filename,
        slugCallback: function (filename) {
            return filename.replace('(', '_').replace(']', '_');
        },
        fileActionSettings: {
            showRemove: true,
            showUpload: false,
            showZoom: true,
            showDrag: true,
        },
    });

</script>