<g:set var='securityConfig' value='${applicationContext.springSecurityService.securityConfig}'/>
<html>
<head>
	<meta name="layout" content="main"/>
	<s2ui:title messageCode='spring.security.ui.login.title'/>
	<asset:stylesheet src='spring-security-ui-auth.css'/>
</head>
<body>
<div class="sufee-login d-flex align-content-center flex-wrap">
	<div class="container">
		<div class="login-content">
			<div class="login-logo">
				<a href="index.html">
					<img class="align-content" src="http://localhost:8080/assets/logo.png" alt="">
				</a>
			</div>
			<div class="login-form">
				<s2ui:form type='login' focus='username'>
					<div class="form-group">
						<label>Email address</label>
						<input type="text" name="${securityConfig.apf.usernameParameter}" id="username" class="form-control" placeholder="Nom d'utilisateur">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="${securityConfig.apf.passwordParameter}" id="password" class="form-control" placeholder="Password">
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="${securityConfig.rememberMe.parameter}" id="remember_me"> Se rappeler de moi
						</label>
					</div>
					<input type="submit" id="loginButton_submit" class="btn btn-success btn-flat m-b-30 m-t-30"/>
				</s2ui:form>
			</div>
		</div>
	</div>
</div>
</body>
</html>
