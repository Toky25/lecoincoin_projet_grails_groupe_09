<!DOCTYPE html>
<head>
    <meta name="layout" content="main"/>
</head>

<div class="clearfix"></div>

<br>

<!-- Orders -->
<div class="orders">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <g:form controller="utilisateur" >
                        <div class="card-body card-block">
                            <input type="hidden" value="0" name="article" id="id">
                            <div class="row form-group">
                                <label class=" form-control-label">Nom d'utilisateur</label>
                                <input type="text" id="titre" name="nom" class="form-control">
                            </div>
                            <div class="row form-group">
                                <label class=" form-control-label">Mot de passe</label>
                                <input type="password" id="descriptionCourte" name="mdp" class="form-control">
                            </div>
                            <div class="row form-group">
                                <label>Rôle de l'utilisateur</label>
                                <select name="role" class="form-control">
                                    <g:each in="${liste}" var="i">
                                        <option value="${i.id}">${i.authority}</option>
                                    </g:each>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <g:actionSubmit type="button" class="btn btn-primary" value="Confirmer" action="save"/>
                        </div>
                    </g:form>
                </div>
            </div> <!-- /.card -->
        </div>  <!-- /.col-lg-8 -->
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#utilisateur").addClass("active");
    });
</script>