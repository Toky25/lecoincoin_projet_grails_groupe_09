<!DOCTYPE html>
<head>
    <meta name="layout" content="main"/>
</head>
<div class="clearfix"></div>

<br>

<!-- Orders -->
<div class="orders">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
               <div class="card-body">
                   <g:form controller="utilisateur">
                       <div class="card-body card-block">
                           <g:hiddenField name="id" value="${utilisateur.id}"/>
                           <g:hiddenField name="password" value="${utilisateur.password}"/>
                           <g:hiddenField name="idRole" value="${liste.id}"/>
                           <div class="row form-group">
                                   <label class=" form-control-label">Nom d'utilisateur</label>
                                   <input type="text" id="titre" name="username" class="form-control" value="${utilisateur.username}">
                           </div>
                           <div class="row form-group">
                               <label>Rôle de l'utilisateur</label>
                               <select name="roleId" class="form-control">
                                   <g:each in="${liste}" var="i">
                                       <g:if test="${idrole.toString().compareTo(i.id.toString())}">
                                           <option value="${i.id}">${i.authority}</option>
                                       </g:if>
                                       <g:else>
                                           <option value="${i.id}" selected>${i.authority}</option>
                                       </g:else>
                                   </g:each>
                               </select>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <g:actionSubmit type="button" class="btn btn-primary" value="Confirmer" action="update"/>
                       </div>
                   </g:form>
               </div>
            </div> <!-- /.card -->
        </div>  <!-- /.col-lg-8 -->
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#utilisateur").addClass("active");
    });
</script>