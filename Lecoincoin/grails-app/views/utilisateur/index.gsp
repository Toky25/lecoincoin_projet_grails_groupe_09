<!DOCTYPE html>
<head>
    <meta name="layout" content="main"/>
</head>
<div class="animated fadeIn">

    <div class="clearfix"></div>

    <br>
    <!-- Orders -->
    <div class="orders">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <g:if test="${flash.message}">
                            <div class="message" style="display: block"></div>
                            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                                <span class="badge badge-pill badge-success">Succès</span>
                                ${flash.message}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        </g:if>

                        <g:if test="${flash.error}">
                            <div class="message" style="display: block"></div>
                            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                                <span class="badge badge-pill badge-danger">Erreur</span>
                                ${flash.error}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        </g:if>

                        <div class="col-sm-12">
                            <h4 class="d-inline-block">Utilisateur</h4>
                            <!--<div class="float-right"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#mediumModal" >Insérer nouveau</button></div>-->
                            <div class="float-right"><a class="btn btn-success" href="${createLink(action: 'redactionUser', controller: 'utilisateur')}" >Insérer nouveau utilisateur</a></div>
                        </div>
                    </div>
                    <div class="card-body--">
                        <div class="table-stats order-table ov-h">
                            <table class="table ">
                                <thead>
                                <tr>
                                    <th class="serial">#</th>
                                    <th>Nom d'utilisateur</th>
                                    <th>Roles</th>
                                    <th>Modifier</th>
                                    <th>Supprimer</th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${liste}" status="i" var="utilisateur">
                                    <tr>
                                        <td class="serial">${i+1}</td>
                                        <td>${utilisateur.username}</td>
                                        <g:if test="${utilisateur.authorities.first().id == 1}">
                                            <td>ROLE_ADMIN</td>
                                        </g:if>
                                        <g:elseif test="${utilisateur.authorities.first().id == 2}">
                                            <td>ROLE_MODO</td>
                                        </g:elseif>
                                        <g:elseif test="${utilisateur.authorities.first().id == 3}">
                                            <td>ROLE_USER</td>
                                        </g:elseif>
                                        <td><a href="${createLink(action: 'modificationUser', controller: 'utilisateur', params: [utilisateurId:utilisateur.id])}" class="btn btn-primary">Modifier les informations</a></td>
                                        <td><a href="${createLink(action: 'delete', controller: 'utilisateur', params:[id: "${utilisateur.id}"] )}" onclick="return confirm('Voulez vous réellement supprimer?');" class="btn btn-danger">Supprimer</a></td>
                                    </tr>
                                </g:each>
                                </tbody>
                            </table>
                        </div> <!-- /.table-stats -->
                    </div>
                </div> <!-- /.card -->
            </div>  <!-- /.col-lg-8 -->
        </div>
    </div>
    <div class="float-right">
        <ul class="pagination">
            <g:if test="${Integer.valueOf(params.page)-1>0}">
                <li class="page-item"><a class="page-link" href="${createLink(action: 'index', controller: 'utilisateur', params: [page:"${Integer.valueOf(params.page)-1}"])}">Précédent</a></li>
            </g:if>
            <g:else>
                <li class="page-item disabled"><a class="page-link" href="">Précédent</a></li>
            </g:else>

            <g:set var="counter" value="${0}"/>
            <g:while test="${counter < nbBoutons}">
                <g:set var="counter" value="${counter+1}"/>
                <g:if test="${Integer.valueOf(counter)==Integer.valueOf(params.page)}">
                    <li class="page-item active"><a class="page-link" href="${createLink(action: 'index', controller: 'utilisateur', params: [page:"${counter}"])}">${counter}</a></li>
                </g:if>
                <g:else>
                    <li class="page-item"><a class="page-link" href="${createLink(action: 'index', controller: 'utilisateur', params: [page:"${counter}"])}">${counter}</a></li>
                </g:else>
            </g:while>

            <g:if test="${Integer.valueOf(params.page)+1<=nbBoutons}">
                <li class="page-item"><a class="page-link" href="${createLink(action: 'index', controller: 'utilisateur', params: [page:"${Integer.valueOf(params.page)+1}"])}">Suivant</a></li>
            </g:if>
            <g:else>
                <li class="page-item disabled"><a class="page-link" href="">Suivant</a></li>
            </g:else>
        </ul>
    </div>
    <!-- /.orders -->
    <!-- To Do and Live Chat -->
</div>

<script>
    $(document).ready(function(){
        $("#utilisateur").addClass("active");
    });
</script>

</body>
</html>