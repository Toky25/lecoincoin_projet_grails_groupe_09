<!DOCTYPE html>
<head>
    <meta name="layout" content="main"/>
</head>
<header id="header" class="header">
    <div class="top-left">
        <div class="navbar-header">
            <a class="navbar-brand" href="./"><img src="http://localhost:8080/assets/logo.png" alt="Logo"></a>
            <a class="navbar-brand hidden" href="./"><img src="http://localhost:8080/assets/logo2.png" alt="Logo"></a>
            <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
        </div>
    </div>
    <div class="top-right">
        <div class="header-menu">
            <div class="header-left">
                <div class="form-inline">
                    <form class="search-form">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                        <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                    </form>
                </div>


                <div id="profilconnex" class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="user-avatar rounded-circle" src="http://localhost:8080/assets/admin.jpg" alt="User Avatar">
                    </a>

                    <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="${createLink(action: 'logout', controller: 'logout')}"><i class="fa fa-power -off"></i>Se déconnecter</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $.get("http://localhost:8080/logout/ifconnected", function(data, status){
                var status = data.connected.toString()
                if(status==="yes") {
                    $('#profilconnex').show();
                }else if(status==="no"){
                    $('#profilconnex').hide();
                }
            });
        });
    </script>
</header>